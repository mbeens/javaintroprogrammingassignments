package section3_apis.part1_interfaces;

public class CombinerFactory {
    /**
     * This method serves a StringCombiner that will surround the given arguments with double quotes,
     * separated by spaces and the result surrounded by single quotes.
     *
     * For example, the call
     *      combiner.combine("one", "two")
     * will return '"one" "two"'
     * @return quotedCombiner
     */
    static StringCombiner getQuotedCombiner() {
        //YOUR CODE HERE (and remove the throw statement)

        class quotedCombiner implements StringCombiner{

            @Override
            public String combine(String first, String second) {
                return "'\"" + first + "\" \"" + second + "\"'";
            }
        }
        return new quotedCombiner();
    }


    /**
     * This method serves a StringCombiner that will combine the given arguments reversed and original,
     * concatenated with a space in between.
     *
     * For example,
     *      combiner.combine("one", "two")
     * will return "oneeno twoowt"
     *
     * (the quotes are not included in the return value).
     * @return reversedCombiner
     */
    static StringCombiner getReversedCombiner() {
        //YOUR CODE HERE (and remove the throw statement)

        class reversedCombiner implements StringCombiner{

            @Override
            public String combine(String first, String second) {
                String ref_first = new StringBuilder(first).reverse().toString();
                String ref_second = new StringBuilder(second).reverse().toString();
                return first + ref_first + " " + second + ref_second;
            }
        }
        return new reversedCombiner();
    }

    /**
     * <strong>Challenge!</strong>
     * This method serves a StringCombiner that will combine the given arguments so that the characters of both
     * arguments are converted to their ASCII values and then the summed. These numbers are combined with a space
     * in between and returned.
     *
     * For example,
     *      combiner.combine("one", "two")
     * will return "322 346"
     * Because 111 + 110 + 101 = 322 and 116 + 119 + 111 = 346.
     *
     * Hint: a char IS AN integer behind the scenes
     *
     * @return reversedCombiner
     */
    static StringCombiner getAsciiSumCombiner() {
        //YOUR CODE HERE (and remove the throw statement)

        class reversedCombiner implements StringCombiner{
            private int wordSum (String word){
                int sum = 0;
                for (char character : word.toCharArray()){
                    sum += character;
                }
                return sum;
            }

            @Override
            public String combine(String first, String second) {
                int sum_first = wordSum(first);
                int sum_second = wordSum(second);
                return sum_first + " " + sum_second;
            }
        }

        return new reversedCombiner();
    }

}
