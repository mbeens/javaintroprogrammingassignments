package section3_apis.part1_interfaces;

import java.util.Arrays;

public class CaesarCypher implements EncryptionEngine {

    @Override
    public String encrypt(String message) {
        //YOUR CODE HERE (and remove the throw statement)
        StringBuilder encrypted = new StringBuilder("");
        for (char character : message.toCharArray()){
            if (character < 65 || character > 90 && character < 97 || character > 122) {
                encrypted.append(character);
            } else{
                if (Arrays.asList('A', 'a', 'B', 'b', 'C', 'c').contains(character)){
                    character = (char) (character + 26);
                }
                encrypted.append((char) (character - 3));
            }
        }
        return encrypted.toString();
    }

    @Override
    public String decrypt(String encryptedMessage) {
        //YOUR CODE HERE (and remove the throw statement)
        StringBuilder decrypted = new StringBuilder("");
        for (char character : encryptedMessage.toCharArray()){
            if (character < 65 || character > 90 && character < 97 || character > 122) {
                decrypted.append(character);
            } else {
                if (Arrays.asList('X', 'x', 'Y', 'y', 'Z', 'z').contains(character)){
                    character = (char) (character - 26);
                }
                decrypted.append((char) (character + 3));
            }
        }

        return decrypted.toString();
    }
}
